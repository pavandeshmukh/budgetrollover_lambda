'use strict';
const request = require('request-promise');

exports.handler = async (event) => {

  let options = {
    method: 'POST',
    url: `https://console-stage.mlprivate.net/BudgetRollover/updateLeadgenMonthlyBudget`,
    //url: `http://172.27.176.34:8080/opportunity/getOpportunityProductById`,  // To Do : Need to remove this as this is testing URL
    json: true,
    headers: '{"Authorization": "9749ae5f-9f1f-4c28-a0f5-7163c5d3ca54"}' // To Do : Update token with Okta token once available    
  };

  const { responsebody } = await request(options);
  const response = {
    statusCode: 200,
    body: responsebody
  };
  return response;
};